/* eslint no-param-reassign: ["error", { "props": false }] */
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    pastCollection: 0,
    newCollection: 0,
    maxCollection: 0,
    openNav: false,
    language: 'en',
  },

  getters: {
    openNav: state => state.openNav,
    activeCollection: state => state.newCollection,
    transitionDirection: (state) => {
      const direction = state.pastCollection > state.newCollection ? '' : 'reverse';
      return direction;
    },
  },

  // Todo: refactor by component store when there is too much here
  actions: {
    toggleNav({ commit }) {
      commit('toggleNav');
    },

    initCollection({ commit }, collectionMax) {
      commit('initCollection', collectionMax);
    },

    changeCollection({ commit, state }, collectionId) {
      commit('mutateCollection', collectionId);
    },

    previousCollection({ commit, state }) {
      let collectionId = state.newCollection - 1;
      if (state.newCollection < 0) {
        collectionId = state.maxCollection;
      }
      commit('mutateCollection', collectionId);
    },

    nextCollection({ commit, state }) {
      let collectionId = state.newCollection + 1;
      if (state.newCollection === state.maxCollection) {
        collectionId = 0;
      }
      commit('mutateCollection', collectionId);
    },
  },

  mutations: {
    initCollection(state, collectionMax) {
      state.maxCollection = collectionMax;
    },

    mutateCollection(state, collectionId) {
      state.pastCollection = state.newCollection;
      state.newCollection = collectionId;
    },

    toggleNav(state) {
      state.openNav = !state.openNav;
    },
  },
});
