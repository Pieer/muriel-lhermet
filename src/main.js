// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueI18n from 'vue-i18n';
import VueCloudinary from 'vue-cloudinary';
import App from './App';
import router from './router';
import store from './store/store';

import messages from './data/global.json';

Vue.config.productionTip = false;
Vue.use(VueI18n);
Vue.use(VueCloudinary, {
  cloud_name: 'ahoy',
  api_key: '239826562185531',
  cdn_subdomain: true,
});

const i18n = new VueI18n({
  locale: 'en',
  messages,
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  i18n,
  template: '<App/>',
  components: { App },
});
