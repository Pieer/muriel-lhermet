import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/pages/Home';
import Artist from '@/pages/Artist';
import Collections from '@/pages/Collections';
import Collection from '@/pages/Collection';
import Workshop from '@/pages/Workshop';
import Exhibitions from '@/pages/Exhibitions';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/home',
      name: 'Home',
      component: Home,
    },
    {
      path: '/collections',
      name: 'Collections',
      component: Collections,
    },
    {
      path: '/collection/:id',
      name: 'Collection',
      component: Collection,
    },
    {
      path: '/artist',
      name: 'Artist',
      component: Artist,
    },
    {
      path: '/workshop',
      name: 'Workshop',
      component: Workshop,
    },
    {
      path: '/exhibitions',
      name: 'Exhibitions',
      component: Exhibitions,
    },
    { path: '*', redirect: '/home' },
  ],
});
